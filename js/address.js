$(document).ready(function() {
  $("#info form").submit(function(event) {
    event.preventDefault();

    var address = $("input#address").val();
    var name = $("input#name").val();
    var city = $("input#city").val();
    var state = $("input#state").val();
    var zip = $("input#zip").val();

    $(".address").text(address);
    $(".name").text(name);
    $(".city").text(city);
    $(".state").text(state);
    $(".zip").text(zip);

    $("#output").removeClass(".hidden");
  });
});
