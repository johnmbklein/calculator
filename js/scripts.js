
var add = function(number1, number2) {
	return number1 + number2;
};

var subtract = function(number1, number2) {
	return number1 - number2;
};

var multiply = function(number1, number2) {
  return number1 * number2;
};

var divide = function(number1, number2) {
  	return number1 / number2;
};

var checkInput = function(input, target){
	if (!input) {
		$("#"+target).parent().addClass("has-error");
		$("#"+target+"-help-text").text("Error: please enter a number.");
	} else {
		$("#"+target).parent().removeClass("has-error");
		$("#"+target+"-help-text").empty("Error: please enter a number.");
	}
};

var calculate = function(){
	var number1 = parseFloat($("#number1").val());
	var number2 = parseFloat($("#number2").val());
	var operation = $("#operation").val();
	var result;
	checkInput(number1, "number1");  //Is there a better way to do this
	checkInput(number2, "number2");
	switch (operation) {
		case 'add':
			result = add(number1, number2);
			break;
		case "subtract":
			result = subtract(number1, number2);
			break;
		case "multiply":
			result = multiply(number1, number2);
			break;
		case "divide":
			result = divide(number1, number2);
			break;
		default:
			result = "error";
	}
	$("#result").text(result);
}

$(document).ready(function() {
  $("form#calculator").submit(function(event) {
    event.preventDefault();
		calculate();
  });
});
